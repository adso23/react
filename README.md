## Form-React

## Description
The objective of this project is to put into practice the skills and knowledge acquired during training in the use of frameworks such as REACT. Through the creation of components that allow us to carry out multiple developments; in this case, the creation of a registration form. This project is 100% for learning purposes.


## Visuals
![Form](https://gitlab.com/adso23/react/-/raw/main/src/img/Form.png?ref_type=heads)


## Installation
# **Getting Started with React**

1. ## *Check the installed versions of Node.js and npm.*

```BASH
node --version
npm -v

```
2. ## *Install create react app (to facilitate the creation of React projects)*

```BASH
npm install -g create-react-app

```

3. ## *Create the new React application and assign a name (Formulario-react)*

```BASH
npx create-react-app Formulario-react

```

4. ## *Navigate to the project directory*

5. ## *Install the necessary packages*

```BASH
npm install

```

6. ## *Run the project*

```BASH
npm start

```

## Usage

"Learning."

## Support
@JCodev


## License
MIT

