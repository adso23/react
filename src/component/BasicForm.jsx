
import React, { useState } from 'react';
import '../css/styles.css';

export const FormBasic = () => {
  const [nombre, setNombre] = useState('');
  const [apellido, setApellido] = useState('');
  const [edad, setEdad] = useState('');
  const [centro, setCentro] = useState('');
  const [ficha, setFicha] = useState('');
  const [modalidad, setModalidad] = useState(''); 
  const [mensaje, setMensaje] = useState('');
  const [messageType, setMessageType] = useState('');
  const [messageVisible, setMessageVisible] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();

    //Validaciones
    if (!nombre || !apellido || !edad || !centro || !ficha || !modalidad) {
      showErrorMessage('Por favor, llene todos los campos.');
    } else if (!/^[a-zA-Z]+$/.test(nombre) || !/^[a-zA-Z]+$/.test(apellido)) {
      showErrorMessage('Nombre y apellido no recibe caracteres numéricos.');
    } else if (!/^\d+$/.test(ficha)) {
      showErrorMessage('Ficha solo puede contener valores numéricos.');
    } else {
      showSuccessMessage('Datos enviados exitosamente.');
    }

    //Limpiar los campos del formulario
    setNombre('');
    setApellido('');
    setEdad('');
    setCentro('');
    setFicha('');
    setModalidad('');
  };

  // Función para mostrar mensajes de error
  const showErrorMessage = (message) => {
    setMensaje(message);
    setMessageType('error');
    showMessage();
  };

  // Función para mostrar mensajes exitosos
  const showSuccessMessage = (message) => {
    setMensaje(message);
    setMessageType('success');
    showMessage();
  };

  // Función para mostrar el contenedor de mensajes
  const showMessage = () => {
    setMessageVisible(true);
    // Ocultar el mensaje después de 5 segundos
    setTimeout(() => {
      setMessageVisible(false);
    }, 5000);
  };

  return (
    <div className="form-container">
      <form onSubmit={handleSubmit}>
        <label>
          <strong>Nombre:</strong>
          <input type="text" value={nombre} onChange={(z) => setNombre(z.target.value)} />
        </label>
        <br />
        <label>
          <strong>Apellido:</strong>
          <input type="text" value={apellido} onChange={(z) => setApellido(z.target.value)} />
        </label>
        <br />
        <label htmlFor="edad">
          <strong>Edad:</strong>
          <select value={edad} onChange={(z) => setEdad(z.target.value)}>
            <option value="">Selecciona</option>
            {[...Array(70)].map((_, index) => (
              <option key={index} value={index + 1}>
                {index + 14}
              </option>
            ))}
          </select>
        </label>
        <br />
        <label htmlFor="centro">
          <strong>Centro de Formación:</strong>
          <select value={centro} onChange={(z) => setCentro(z.target.value)}>
            <option value="">Selecciona</option>
            <option value="servicios">Centro de Servicios Empresariales y Turísticos</option>
            <option value="diseno">Centro Industrial del Diseño y la Manufactura</option>
            <option value="mantenimiento">Centro Industrial de Mantenimiento Integral</option>
          </select>
        </label>
        <br />
        <label htmlFor="modalidad">
          <strong>Modalidad:</strong>
          <select value={modalidad} onChange={(z) => setModalidad(z.target.value)}>
            <option value="">Selecciona</option>
            <option value="presencial">Presencial</option>
            <option value="virtual">Virtual</option>
            <option value="distancia">A Distancia</option>
          </select>
        </label>
        <br />
        <label htmlFor="ficha">
          <strong>Ficha:</strong>
          <input type="text" value={ficha} onChange={(z) => setFicha(z.target.value)} />
        </label>
        <br />
        <button type="submit"><strong>Enviar</strong></button>
        <div className={`message-container ${messageType} ${messageVisible ? 'show' : ''}`}>
          {mensaje}
        </div>
      </form>
    </div>
  );
};





